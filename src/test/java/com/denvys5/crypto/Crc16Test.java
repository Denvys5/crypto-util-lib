/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Crc16Test {

    @Test
    public void testCRC16_1(){
        Crc16 crc16 = new Crc16();
        byte[] sample = {49,50,51,52,53,54,55,56,57};
        crc16.update(sample);
        assertEquals(0x31C3, crc16.getValue());
    }

    @Test
    public void testCRC16_2(){
        Crc16 crc16 = new Crc16();
        byte[] sample = "123456789".getBytes(StandardCharsets.UTF_8);
        crc16.update(sample);
        assertEquals(0x31C3, crc16.getValue());
    }

    @Test
    public void testCrcOffset(){
        Crc16 crc16 = new Crc16();
        byte[] sample = "0123456789".getBytes(StandardCharsets.UTF_8);
        crc16.update(sample, 1, sample.length-1);
        assertEquals(0x31C3, crc16.getValue());
    }

    @Test
    public void testTable(){
        assertArrayEquals(new Crc16().getCrcTable(), Crc16.calculateTable((short) 0x1021));
    }

    @Test
    public void generateTable(){
        short[] table = Crc16.calculateTable((short) 0x1021);
        for(int i = 0; i < 32; i++){
            for(int j = 0; j < 8; j++){
                System.out.print("(short) 0x" + String.format("%04X", table[i*8+j]) + ", ");
            }
            System.out.println();
        }
    }

    @Test
    public void testGetCRC_1() {
        Crc16 crc16 = new Crc16();
        byte[] sample = "123456789".getBytes(StandardCharsets.UTF_8);
        assertEquals(0x31C3, crc16.getCRC(sample));
    }

    @Test
    public void testGetCRC_2() {
        Crc16 crc16 = new Crc16();
        byte[] sample = {49,50,51,52,53,54,55,56,57};
        assertEquals(0x31C3, crc16.getCRC(sample));
    }

    @Test
    public void testOffset() {
        Crc16 crc16 = new Crc16();
        byte[] sample = "0123456789".getBytes(StandardCharsets.UTF_8);
        assertEquals(0x31C3, crc16.getCRC(sample, 1, sample.length-1));
    }
}
