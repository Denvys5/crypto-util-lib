/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

import javax.crypto.SecretKey;
import java.lang.reflect.Field;
import java.security.KeyPair;

import static org.junit.jupiter.api.Assertions.*;

public class CryptoServiceTest {

    @Test
    public void testKeyPair() throws Exception {
        ICryptoService cryptoService = new CryptoService();
        cryptoService.generateKeyPair();

        System.out.println(cryptoService.getPublicEcdhKey());
        System.out.println(cryptoService.getPublicEcdhKeyB64());
        assertArrayEquals(EcdhUtil.savePublicKey(cryptoService.getPublicEcdhKey()), Base64.decodeBase64(cryptoService.getPublicEcdhKeyB64()));

        System.out.println(cryptoService.getPublicEcdsaKey());
    }


    @Test
    public void testKeys(){
        ICryptoService cryptoService = new CryptoService();
        cryptoService.generateKeyPair();
    }

    @Test
    public void testUniqueKeys() throws Exception {
        String sampleString = "Hello world";

        //Create 2 points
        ICryptoService cryptoService1 = new CryptoService();
        cryptoService1.generateKeyPair();

        ICryptoService cryptoService2 = new CryptoService();
        cryptoService2.generateKeyPair();

        Field f1 = cryptoService1.getClass().getDeclaredField("ecdhKeyPair"); //NoSuchFieldException
        f1.setAccessible(true);
        KeyPair keyPair1 = (KeyPair) f1.get(cryptoService1);
        System.out.println("Private 1: " + Base64.encodeBase64String(EcdhUtil.savePrivateKey(keyPair1.getPrivate())));
        System.out.println("Public 1: " + Base64.encodeBase64String(EcdhUtil.savePublicKey(keyPair1.getPublic())));

        Field f2 = cryptoService2.getClass().getDeclaredField("ecdhKeyPair"); //NoSuchFieldException
        f2.setAccessible(true);
        KeyPair keyPair2 = (KeyPair) f2.get(cryptoService2);
        System.out.println("Private 2: " + Base64.encodeBase64String(EcdhUtil.savePrivateKey(keyPair2.getPrivate())));
        System.out.println("Public 2: " + Base64.encodeBase64String(EcdhUtil.savePublicKey(keyPair2.getPublic())));

        //Create keys for 2nd point
        String publicKey1 = cryptoService1.getPublicEcdhKeyB64();
        SecretKey secretKey2 = cryptoService2.getSecretEcdhKey(publicKey1);
        System.out.println("Secret Key 2: " + Base64.encodeBase64String(secretKey2.getEncoded()));
        //Encrypt on 2nd point
        String encrypted = cryptoService2.encryptString(sampleString, secretKey2);

        //Create keys for 1st point
        String publicKey2 = cryptoService2.getPublicEcdhKeyB64();
        SecretKey secretKey1 = cryptoService1.getSecretEcdhKey(publicKey2);
        System.out.println("Secret Key 1: " + Base64.encodeBase64String(secretKey1.getEncoded()));
        //Decrypt on 1st point
        String decrypted = cryptoService1.decryptString(encrypted, secretKey1);

        assertEquals(sampleString, decrypted);
    }

    @Test
    public void testExchange() throws Exception {
        String sampleString = "Hello world";
        System.out.println("Sample: " + sampleString);

        //Create 2 points
        ICryptoService cryptoService1 = new CryptoService();
        cryptoService1.generateKeyPair();

        ICryptoService cryptoService2 = new CryptoService();
        cryptoService2.generateKeyPair();

        //Create keys for 2nd point
        String publicKey1 = cryptoService1.getPublicEcdhKeyB64();
        System.out.println("Public Key 1: " + publicKey1);
        SecretKey secretKey2 = cryptoService2.getSecretEcdhKey(publicKey1);
        System.out.println("Secret Key 2: " + Base64.encodeBase64String(secretKey2.getEncoded()));
        //Encrypt on 2nd point
        String encrypted = cryptoService2.encryptString(sampleString, secretKey2);
        System.out.println("Encrypted: " + encrypted);

        //Create keys for 1st point
        String publicKey2 = cryptoService2.getPublicEcdhKeyB64();
        System.out.println("Public Key 2: " + publicKey2);
        SecretKey secretKey1 = cryptoService1.getSecretEcdhKey(publicKey2);
        System.out.println("Secret Key 1: " + Base64.encodeBase64String(secretKey1.getEncoded()));
        //Decrypt on 1st point
        String decrypted = cryptoService1.decryptString(encrypted, secretKey1);
        System.out.println("Decrypted: " + decrypted);

        assertEquals(sampleString, decrypted);
    }

    @Test
    public void testSigning(){
        String sampleString = "Hello world!";

        ICryptoService cryptoService1 = new CryptoService();
        cryptoService1.generateKeyPair();

        String publicKey = cryptoService1.getPublicEcdsaKeyB64();
        String signature = cryptoService1.signString(sampleString);

        ICryptoService cryptoService2 = new CryptoService();
        assertTrue(cryptoService2.verifyString(sampleString, signature, cryptoService2.getPublicEcdsaKey(publicKey)));
    }

    @Test
    public void testInvalidSigning(){
        String sampleString = "Hello world!";

        ICryptoService cryptoService1 = new CryptoService();
        cryptoService1.generateKeyPair();

        String publicKey = cryptoService1.getPublicEcdsaKeyB64();
        String signature = cryptoService1.signString(sampleString);

        String sampleString2 = "Hello!";
        ICryptoService cryptoService2 = new CryptoService();
        assertFalse(cryptoService2.verifyString(sampleString2, signature, cryptoService2.getPublicEcdsaKey(publicKey)));
    }

    @Test
    public void testCombine(){
        String sampleString = "Hello world!";
        System.out.println("Sample: " + sampleString);

        ICryptoService cryptoService1 = new CryptoService();
        cryptoService1.generateKeyPair();
        String publicKey1 = cryptoService1.getPublicEcdhKeyB64();
        System.out.println("Public Key 1: " + publicKey1);
        ICryptoService cryptoService2 = new CryptoService();
        cryptoService2.generateKeyPair();
        String publicKey2 = cryptoService2.getPublicEcdhKeyB64();
        System.out.println("Public Key 2: " + publicKey2);

        String signKey1 = cryptoService1.getPublicEcdsaKeyB64();
        System.out.println("Signing key 1: " + signKey1);
        SecretKey secretKey = cryptoService1.getSecretEcdhKey(publicKey2);

        String encrypted = cryptoService1.encryptString(sampleString, secretKey);
        System.out.println("Encrypted string: " + encrypted);

        String signature = cryptoService1.signString(encrypted);
        System.out.println("Signature: " + signature);

        boolean validate = cryptoService2.verifyString(encrypted, signature, signKey1);
        String decrypted = cryptoService2.decryptString(encrypted, secretKey);

        System.out.println("Valid signature: " + validate);
        System.out.println("Decrypted: " + decrypted);

        assertEquals(sampleString, decrypted);
        assertTrue(validate);
    }
}
