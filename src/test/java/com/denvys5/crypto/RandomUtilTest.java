/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.junit.jupiter.api.Test;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RandomUtilTest {

    @Test
    public void test1(){
        System.out.println(RandomUtil.uniqueLong());
    }

    @Test
    public void test2(){
        int size = 1000;
        AtomicInteger nonUnique = getDuplicates(size);

        System.out.println("Number of duplicates is "+  nonUnique.get());
        assertEquals(0, nonUnique.get());
    }

    @Test
    public void test3(){
        //50K in 12t is 6.692s on 2700X@4.2Ghz
        //50K in 6t is 4.842 on 3600@4.2Ghz
        int size = 5000;
        int threads = 12;
        IntStream.range(0, threads).parallel().forEach((v)->{
            AtomicInteger nonUnique = getDuplicates(size);
            System.out.println("Thread #"+ v + " has " + nonUnique.get() + " duplicates");
            assertEquals(0, nonUnique.get());
        });
    }

    private AtomicInteger getDuplicates(int size) {
        Queue<Long> queue = new PriorityQueue<>();
        for (int i = 0; i < size; i++) {
            queue.add(RandomUtil.uniqueLong());
        }

        AtomicInteger nonUnique = new AtomicInteger();

        for (int i = 0; i < size; i++) {
            Long value = queue.poll();
            queue.stream().filter(e -> e.equals(value)).findAny().ifPresent((a) -> {
                nonUnique.getAndIncrement();
            });
        }
        return nonUnique;
    }
}
