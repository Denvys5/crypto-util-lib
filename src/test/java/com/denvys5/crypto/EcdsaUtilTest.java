/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EcdsaUtilTest {

    @Test
    public void test1() throws Exception {
        KeyPair kp = EcdsaUtil.generateKeys();
        PrivateKey privateKey = kp.getPrivate();
        PublicKey publicKey = kp.getPublic();

        String text = "Hello world";
        String signature = EcdsaUtil.signStringB64(text, privateKey);

        boolean valid = EcdsaUtil.validateSign(text, signature, publicKey);

        assertTrue(valid);
    }

    @Test
    public void test2() throws Exception {
        KeyPair kp = EcdsaUtil.generateKeys();
        PrivateKey privateKey = kp.getPrivate();
        PublicKey publicKey = kp.getPublic();

        String text = "Hello world";
        String signature = EcdsaUtil.signStringB64(text, privateKey);

        String text2 = "Hello world!";
        boolean valid = EcdsaUtil.validateSign(text2, signature, publicKey);

        assertFalse(valid);
    }

    @Test
    public void test3() throws Exception {
        KeyPair kp = EcdsaUtil.generateKeys();
        PrivateKey privateKey = kp.getPrivate();
        PublicKey publicKey = kp.getPublic();

        String text = "Hello world";
        String signature = EcdsaUtil.signStringB64(text, privateKey);


        KeyPair kp2 = EcdsaUtil.generateKeys();
        PublicKey publicKey2 = kp2.getPublic();

        boolean valid = EcdsaUtil.validateSign(text, signature, publicKey2);

        assertFalse(valid);
    }

    @Test
    public void test4() throws Exception {
        KeyPair kp = EcdsaUtil.generateKeys();
        PrivateKey privateKey = kp.getPrivate();
        PublicKey publicKey = kp.getPublic();
        String publicKeyB64 = Base64.encodeBase64String(publicKey.getEncoded());

        String text = "Hello world";
        String signature = EcdsaUtil.signStringB64(text, privateKey);

        boolean valid = EcdsaUtil.validateSign(text, signature, EcdsaUtil.getKey(publicKeyB64));

        assertTrue(valid);
    }
}
