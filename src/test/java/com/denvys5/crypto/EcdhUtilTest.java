/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;

public class EcdhUtilTest {

    @Test
    public void test1() throws Exception {
        KeyPairGenerator kpgen = KeyPairGenerator.getInstance("ECDH", EcdhUtil.provider);
        kpgen.initialize(new ECGenParameterSpec("prime192v1"), new SecureRandom());
        KeyPair pairA = kpgen.generateKeyPair();
        KeyPair pairB = kpgen.generateKeyPair();
        System.out.println("Alice: " + pairA.getPrivate());
        System.out.println("Alice: " + pairA.getPublic());
        System.out.println("Bob:   " + pairB.getPrivate());
        System.out.println("Bob:   " + pairB.getPublic());
        byte[] dataPrvA = EcdhUtil.savePrivateKey(pairA.getPrivate());
        byte[] dataPubA = EcdhUtil.savePublicKey(pairA.getPublic());
        byte[] dataPrvB = EcdhUtil.savePrivateKey(pairB.getPrivate());
        byte[] dataPubB = EcdhUtil.savePublicKey(pairB.getPublic());
        System.out.println("Alice Prv: " + Hex.encodeHexString(dataPrvA));
        System.out.println("Alice Pub: " + Hex.encodeHexString(dataPubA));
        System.out.println("Bob Prv:   " + Hex.encodeHexString(dataPrvB));
        System.out.println("Bob Pub:   " + Hex.encodeHexString(dataPubB));
        byte[] secretA = EcdhUtil.doECDH(dataPrvA, dataPubB).getEncoded();
        byte[] secretB = EcdhUtil.doECDH(dataPrvB, dataPubA).getEncoded();
        System.out.println("Alice's secret: " + Hex.encodeHexString(secretA));
        System.out.println("Bob's secret: " + Hex.encodeHexString(secretB));
    }
}
