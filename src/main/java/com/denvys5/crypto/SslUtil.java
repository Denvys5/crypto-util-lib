/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/*
 * Constantly copy-pasting code from StackOverflow for 5 hours to get down to this.
 * This was a painful and miserable journey through legacy and deprecated implementations of .crt certificate readers
 */
public class SslUtil {
    public static SSLContext getSSLContext(InputStream caCrtFile, InputStream crtFile, InputStream keyFile, final String password) {
        try{
            Security.addProvider(new BouncyCastleProvider());

            X509Certificate caCert =
                    SslUtil.getCertificate(caCrtFile);
            X509Certificate cert =
                    SslUtil.getCertificate(crtFile);
            InputStreamReader inputStreamReader = new InputStreamReader(keyFile);
            PEMParser parser = new PEMParser(inputStreamReader);
            PEMKeyPair kp = (PEMKeyPair) parser.readObject();

            return getSslContext(password, caCert, cert, kp);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static SSLSocketFactory getSocketFactory(InputStream caCrtFile, InputStream crtFile, InputStream keyFile, final String password) {
        return getSSLContext(caCrtFile, crtFile, keyFile, password).getSocketFactory();
    }

    public static SSLSocketFactory getSocketFactory(final String caCrtFilePath, final String crtFilePath, final String keyFilePath, final String password) {
        try{
            Security.addProvider(new BouncyCastleProvider());

            X509Certificate caCert =
                    SslUtil.getCertificate(caCrtFilePath);
            X509Certificate cert =
                    SslUtil.getCertificate(crtFilePath);
            FileReader fileReader = new FileReader(keyFilePath);
            PEMParser parser = new PEMParser(fileReader);
            PEMKeyPair kp = (PEMKeyPair) parser.readObject();

            return getSslContext(password, caCert, cert, kp).getSocketFactory();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static SSLContext getSslContext(String password, X509Certificate caCert, X509Certificate cert, PEMKeyPair kp) throws IOException,
            KeyStoreException,
            NoSuchAlgorithmException,
            CertificateException,
            UnrecoverableKeyException, KeyManagementException {


        PrivateKeyInfo info = kp.getPrivateKeyInfo();

        PrivateKey rdKey = new JcaPEMKeyConverter().setProvider("BC")
                .getPrivateKey(info);

        // CA certificate is used to authenticate server
        KeyStore caKs = KeyStore.getInstance(KeyStore.getDefaultType());
        caKs.load(null, null);
        caKs.setCertificateEntry("ca-certificate", caCert);
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(caKs);

        // client key and certificates are sent to server so it can authenticate us
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(null, null);
        ks.setCertificateEntry("certificate", cert);
        ks.setKeyEntry("private-key", rdKey, password.toCharArray(), new java.security.cert.Certificate[]{cert});
        KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(ks, password.toCharArray());

        // finally, create SSL socket factory
        SSLContext context = SSLContext.getInstance("TLSv1");
        context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        return context;
    }

    public static X509Certificate getCertificate(InputStream pemfile) {
        X509Certificate cert = null;
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(pemfile);
            final PemReader certReader = new PemReader(inputStreamReader);
            final PemObject certAsPemObject = certReader.readPemObject();
            if (!certAsPemObject.getType().equalsIgnoreCase("CERTIFICATE") && !certAsPemObject.getType().contains("CERTIFICATE")) {
                throw new Exception("Certificate file does not contain a certificate but a " + certAsPemObject.getType());
            }
            final byte[] x509Data = certAsPemObject.getContent();
            final CertificateFactory fact = CertificateFactory.getInstance("X509");
            cert = (X509Certificate) fact.generateCertificate(new ByteArrayInputStream(x509Data));
            if (!(cert instanceof X509Certificate)) {
                throw new Exception("Certificate file does not contain an X509 certificate");
            }

        }catch (Exception e) {
            System.err.println("#Exception :");
            e.printStackTrace();
        }
        return cert;
    }

    public static X509Certificate getCertificate(String pemfile) throws IOException {
        FileInputStream fis = new FileInputStream(pemfile);
        return getCertificate(fis);
    }

    public KeyPair decodeKeys(byte[] privKeyBits,byte[] pubKeyBits)
            throws InvalidKeySpecException, NoSuchAlgorithmException {
        KeyFactory keyFactory=KeyFactory.getInstance("RSA");
        PrivateKey privKey=keyFactory.generatePrivate(new
                PKCS8EncodedKeySpec(privKeyBits));
        PublicKey pubKey=keyFactory.generatePublic(new
                X509EncodedKeySpec(pubKeyBits));
        return new KeyPair(pubKey,privKey);
    }
}
