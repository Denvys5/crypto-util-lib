/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECParameterSpec;

import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class EcdsaUtil {
    public static BouncyCastleProvider provider = new BouncyCastleProvider();

    public static String signStringB64(String text, PrivateKey privateKey) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException {
        return Base64.encodeBase64String(signString(text, privateKey));
    }

    private static byte[] signString(String text, PrivateKey privateKey) throws SignatureException, InvalidKeyException, NoSuchAlgorithmException {
        Signature ecdsaSign = Signature.getInstance("SHA256withECDSA", provider);
        ecdsaSign.initSign(privateKey);
        ecdsaSign.update(text.getBytes(StandardCharsets.UTF_8));
        return ecdsaSign.sign();
    }

    public static boolean validateSign(String text, String signatureB64, PublicKey publicKey) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        Signature ecdsaVerify = Signature.getInstance("SHA256withECDSA", provider);
        ecdsaVerify.initVerify(publicKey);
        ecdsaVerify.update(text.getBytes(StandardCharsets.UTF_8));
        return ecdsaVerify.verify(Base64.decodeBase64(signatureB64));
    }

    public static PublicKey getKey(String key) throws DecoderException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] byteKey = Base64.decodeBase64(key);
        X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
        KeyFactory kf = KeyFactory.getInstance("ECDSA", provider);

        return kf.generatePublic(X509publicKey);
    }

    public static KeyPair generateKeys() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        ECParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("B-571");
        KeyPairGenerator g = KeyPairGenerator.getInstance("ECDSA", provider);
        g.initialize(ecSpec, new SecureRandom());
        return g.generateKeyPair();
    }

}
