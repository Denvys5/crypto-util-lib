/*
 * BSD 3-Clause License
 *
 *                            Copyright (c) 2021, Denvys5
 *                            All rights reserved.
 *
 *                            Redistribution and use in source and binary forms, with or without
 *                            modification, are permitted provided that the following conditions are met:
 *
 *                            1. Redistributions of source code must retain the above copyright notice, this
 *                               list of conditions and the following disclaimer.
 *
 *                            2. Redistributions in binary form must reproduce the above copyright notice,
 *                               this list of conditions and the following disclaimer in the documentation
 *                               and/or other materials provided with the distribution.
 *
 *                            3. Neither the name of the copyright holder nor the names of its
 *                               contributors may be used to endorse or promote products derived from
 *                               this software without specific prior written permission.
 *
 *                            THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *                            AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *                            IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *                            DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 *                            FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 *                            DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *                            SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *                            CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 *                            OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *                            OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package com.denvys5.crypto;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.util.Objects;

public class CryptoService implements ICryptoService{
    private KeyPair ecdhKeyPair;
    private KeyPair ecdsaKeyPair;

    @Override
    public PublicKey getPublicEcdhKey() {
        if(Objects.nonNull(ecdhKeyPair))
            return ecdhKeyPair.getPublic();
        else
            return null;
    }

    @Override
    public String getPublicEcdhKeyB64() {
        PublicKey publicKey = getPublicEcdhKey();
        if(Objects.nonNull(publicKey)){
            try {
                return Base64.encodeBase64String(EcdhUtil.savePublicKey(publicKey));
            } catch (Exception ignored) {
                return "";
            }
        }else{
            return "";
        }
    }

    @Override
    public PublicKey getPublicEcdsaKey() {
        if(Objects.nonNull(ecdsaKeyPair))
            return ecdsaKeyPair.getPublic();
        else
            return null;
    }

    @Override
    public String getPublicEcdsaKeyB64() {
        PublicKey publicKey = getPublicEcdsaKey();
        if(Objects.nonNull(publicKey)){
            try {
                return Base64.encodeBase64String(publicKey.getEncoded());
            } catch (Exception ignored) {
                return "";
            }
        }else{
            return "";
        }
    }

    @Override
    public SecretKey getSecretEcdhKey(String otherPk) throws CryptographicException {
        if(Objects.isNull(otherPk) || otherPk.isEmpty()){
            return null;
        }
        try {
            return EcdhUtil.doECDH(EcdhUtil.savePrivateKey(ecdhKeyPair.getPrivate()), Base64.decodeBase64(otherPk));
        } catch (Exception e) {
            throw new CryptographicException(e);
        }
    }

    @Override
    public PublicKey getPublicEcdsaKey(String publicKey) throws CryptographicException {
        try {
            return EcdsaUtil.getKey(publicKey);
        } catch (DecoderException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new CryptographicException(e);
        }
    }

    @Override
    public String decryptString(String strToDecrypt, SecretKey secret) throws CryptographicException{
        if(Objects.isNull(secret))
            return strToDecrypt;
        else {
            try {
                return AesUtil.decrypt(strToDecrypt, secret.getEncoded());
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                throw new CryptographicException(e);
            }
        }
    }

    @Override
    public String decryptString(String strToDecrypt, byte[] secret) throws CryptographicException{
        if(Objects.isNull(secret) || secret.length == 0)
            return strToDecrypt;
        else {
            try {
                return AesUtil.decrypt(strToDecrypt, secret);
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                throw new CryptographicException(e);
            }
        }
    }

    @Override
    public String encryptString(String strToEncrypt, byte[] secret) throws CryptographicException{
        if(Objects.isNull(secret) || secret.length == 0)
            return strToEncrypt;
        else {
            try {
                return AesUtil.encrypt(strToEncrypt, secret);
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                throw new CryptographicException(e);
            }
        }
    }

    @Override
    public String encryptString(String strToEncrypt, SecretKey secret) throws CryptographicException{
        if(Objects.isNull(secret))
            return strToEncrypt;
        else {
            try {
                return AesUtil.encrypt(strToEncrypt, secret.getEncoded());
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException e) {
                throw new CryptographicException(e);
            }
        }
    }

    @Override
    public String signString(String text) throws CryptographicException{
        try {
            return EcdsaUtil.signStringB64(text, ecdsaKeyPair.getPrivate());
        } catch (SignatureException | InvalidKeyException | NoSuchAlgorithmException e) {
            throw new CryptographicException(e);
        }
    }

    @Override
    public boolean verifyString(String text, String signature, PublicKey otherPk) throws CryptographicException{
        try {
            return EcdsaUtil.validateSign(text, signature, otherPk);
        } catch (SignatureException | InvalidKeyException | NoSuchAlgorithmException e) {
            throw new CryptographicException(e);
        }
    }

    @Override
    public boolean verifyString(String text, String signature, String otherPk) throws CryptographicException{
        return verifyString(text, signature, getPublicEcdsaKey(otherPk));
    }

    @Override
    public void generateKeyPair(){
        try {
            ecdhKeyPair = EcdhUtil.generateKeyPair();
        }catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            ecdsaKeyPair = EcdsaUtil.generateKeys();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
